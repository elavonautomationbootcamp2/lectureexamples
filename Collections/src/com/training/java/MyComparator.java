package com.training.java;

import java.util.Comparator;

public class MyComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		int result = 0;
		int v1 = countVowels(o1);
		int v2 = countVowels(o2);

		// if(v1 < v2) {
		// result = -1;
		// } else if(v2 < v1){
		// result = 1;
		// } else {
		// result = 0;
		// }

		return v1 - v2;
		// return result;
	}
	
	int countVowels(String str) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);

			switch (c) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				count++;
			}
		}
		return count;
	}

}
