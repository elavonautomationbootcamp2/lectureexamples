package com.elavon.jtutorial;

public class Person {
	String firstName;
	String lastName;
	
	Person(String fname, String lname) {
		firstName = fname;
		lastName = lname;
	}
	
	@Override
	public String toString() {
		return "[" + lastName + ", " + firstName + "]";
	}

	public static void main(String[] args) {
		Person p1 = new Person("Johnrey","San Miguel");
		System.out.println(p1);

	}

}
