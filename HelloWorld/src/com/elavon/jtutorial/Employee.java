package com.elavon.jtutorial;

public class Employee {
	int id;
	
	@Override
	public boolean equals(Object obj) {
		boolean isEqual = false;
		if(obj instanceof Employee) {
			Employee anotherEmployee = (Employee) obj;
			isEqual = (this.id == anotherEmployee.id);
		} else {
			return false;
		}
		
		return isEqual;
	}
	
	public static void main(String[] args) {
		Employee e1 = new Employee();
		e1.id = 100;
		Employee e2 = new Employee();
		e2.id = 100;
		Employee e3 = new Employee();
		
//		System.out.println(e1.equals(e2));
		System.out.println(e1.equals(e3));
	}

}
