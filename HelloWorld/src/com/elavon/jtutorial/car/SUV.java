package com.elavon.jtutorial.car;


public class SUV extends Car {
	
	
	public static void main(String[] args) {
		SUV suv = new SUV();
		suv.refuel();
		suv.drive();
		System.out.println(suv.maxSpeed);
		System.out.println(suv.capacity);
	}
	
}
