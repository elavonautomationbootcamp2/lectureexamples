package com.elavon.jtutorial.animal;

public class AnimalsDemo {

	public static void main(String[] args) {
		Animal a1 = new Dog();
		Animal a2 = new Bird();
		Animal a3 = new Fish();
		
		a1.speak();
		a2.speak();
		a3.speak();

	}

}
