package com.elavon.jtutorial.animal;

public class Dog implements Animal{

	@Override
	public void speak() {
		System.out.println("Woof!");
	}

}
