package com.elavon.jtutorial.shapes;

public abstract class Shape {
	abstract void draw();
	
	void ping() {}
}
