package com.training.java;

class Mammal {
	String type;
	static int counter;
	
	static {
		System.out.println("static block");
	}
	
	{
		System.out.println("instance block");
	}
} 

class Amphib {
	
}

public class Static {
	int i;
	static int j;
	
	public static void main(String[] args) {
		Mammal m;
		m = new Mammal();
		m.type = "m";
		m.counter++;
		
		System.out.println(Mammal.counter);
		
		Mammal m2 = new Mammal();
		m2.type = "m2";
		m2.counter++;
		
		System.out.println(Mammal.counter);
		
		mm();
		
		Static s = new Static();
		
		InnerClass ic = s.new InnerClass();
	}
	
	static void mm(){
		j++;
	}

	class InnerClass {
		
	}

}
