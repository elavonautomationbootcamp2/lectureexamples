package com.training.java.abstracts;

class Bird extends Animal {

	@Override
	void eat() {
		System.out.println("peak");
	}

	@Override
	void m2() {
		// TODO Auto-generated method stub
		
	}}

abstract class Animal {
	
	/*
	 * This is the way this Animal eats
	 */
	abstract void eat();
	
	void mm1(){
		System.out.println("Animal");
	}
	
	abstract void m2();
	
}



class Dog1 extends Animal {
	void mm1(){
		System.out.println("Dog");
		super.mm1();
	}
	
	@Override
	void m2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void eat() {
		// TODO Auto-generated method stub
		
	}
	
}

abstract class Dog extends Animal {

	abstract void dogM();
}

class Pug extends Dog{

	@Override
	void m2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void dogM() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void eat() {
		// TODO Auto-generated method stub
		
	}}

public class Abstracts {

	public static void main(String[] args) {
		
		Dog1 d = new Dog1();
		d.mm1();
	}

}

interface MyInterface{
	int i = 0;
	void intf();
}

interface Myintf2  {}

class MyCOn1 extends Animal implements MyInterface, Myintf2 {

	@Override
	public void intf() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void m2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void eat() {
		// TODO Auto-generated method stub
		
	}
}
		

