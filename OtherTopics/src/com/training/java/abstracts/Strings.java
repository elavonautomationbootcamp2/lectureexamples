package com.training.java.abstracts;

class Person {
	String name;
	int age;
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Person){
			Person other = (Person) obj;
			return this.name.equals(other.name) && this.age == other.age;
		}
		
		return false;
	}
}

public class Strings {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.name = "Joe";
		
		Person p2 = new Person();
		p2.name = "Joe";
//		p2.age = 5;
		
		System.out.println(p1 == p2);
		System.out.println(p1.equals(p2));
	}

	public static void main23(String[] args) {
		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder("abc");

		StringBuffer sbf = new StringBuffer();

		Object obj = null;

		sb.append("asd").append("123").append(456).append(true).append('4')
				.append(obj);

		sb.append(sb);

		String str = sb.toString() + "";

		System.out.println(sb.toString());
	}

	public static void strings(String[] args) {

		String str = "Hello";

		String str2 = new String("hello");

		// String str2 = "Hello";

		System.out.println(str == str2); // false
		System.out.println(str.equals(str2)); // false
		System.out.println(str.equalsIgnoreCase(str2)); // true

		str = str.replace("e", "E");

		// System.out.println(str);

		String one = "1";
		String two = "2";

		String three = one + two;

		String six = one + two + three;

		one.concat(two);
		one.replace("1", "3");
		one.replaceFirst("e", "R");

		// System.out.println(one);

	}

}
