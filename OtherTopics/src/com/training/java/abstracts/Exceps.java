package com.training.java.abstracts;

class Animal5 {
}

class Dog5 extends Animal5 {
}

class Pug5 extends Dog5 {
}

class NegativeNumberArgumentException extends Exception {
	
	public NegativeNumberArgumentException() {
		super("A Negative Number was passed as a parameter");
	}
	
}

public class Exceps {
	public static String nameExceps;
	public static void main(String[] args)
			throws Exception {

		// try {
		exceptionHandler(-1);
		// } catch (NegativeNumberArgumentException e) {
		// System.out.println("an exception was thrown");
		// }

		System.out.println("END");
	}

	public void runtimeExceps() {
		throw new ArrayIndexOutOfBoundsException();
	}
	
	public static void exceptionThrower(int number)
			throws Exception {
		if (number < 0) {
			throw new NegativeNumberArgumentException();
		}
	}

	public static void exceptionHandler(int num)
			throws Exception {
		exceptionThrower(num);
	}

	public static void basicExceps(String[] args) {
		Animal5 a = new Dog5();
		/*
		 * try {
		 * 
		 * } finally {
		 * 
		 * }
		 */

		try {
			return;
			// Pug5 pug = (Pug5) a;
			//
			// for(int i = 0; i < 10; i++){
			// System.out.println(10 / i);
			// }

		} catch (ArithmeticException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println("2nd catch");
		} finally {
			System.out.println("finally");
		}

		System.out.println("END");
	}

}
