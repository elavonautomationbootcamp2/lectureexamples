package com.training.selenium.selenium_test;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppTest {

	WebDriver driver;

	@Before
	public void setup() {
		System.out.println("weurowie");
		driver = new FirefoxDriver();
		driver.get("http://demoqa.com/registration/");
	}

	@Test
	public void testApp() throws InterruptedException {
		driver.findElement(By.id("name_3_firstname")).sendKeys("Hello World!");

		TimeUnit.SECONDS.sleep(10);
		System.out.println("Yelloe!");
	}

	@After
	public void cleanUp() {
		driver.quit();
	}
}
