package com.elavon.training;

public class MultiDArrays {

	public static void main(String[] args) {
		
		int row = 3;
		int col = 5;
		
		int[][] array2d = new int[col][row];

		array2d[0] = new int[]{1,2,3};
		array2d[1] = new int[]{4,5,6};
		array2d[2] = new int[]{7,8,9};
		
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				System.out.print(array2d[i][j] + " ");
			}
			System.out.println();
		}
	}

}
