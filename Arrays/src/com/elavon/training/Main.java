package com.elavon.training;

class Animal {
	void makeSound(){
		System.out.println("Animal makes sound");
	}
}
class Dog extends Animal {
	void makeSound(){
		System.out.println("Bark");
	}
}
class Cat extends Animal {
	void makeSound(){
		System.out.println("Meow");
	}
}

public class Main {

	private static void method1(int... nums) {
//		for(int i=0; i < nums.length; i++){
//			System.out.println(nums[i]);
//		}
		
		
		
//		for(int n : nums){
//			n = 1;
//		}
//		
//		for(int n : nums){
//			System.out.println(n);
//		}
	}

	public static void main(String... args) {
		
//		System.out.println(args.length);
//		
//		for(String arg : args){
//			System.out.println(arg);
//		}
		
		Animal[] animals = {new Dog(), new Cat(), new Animal()};
		
		for(Animal animal : animals){
			animal.makeSound();
		}
		
		int[][] nums = new int[3][];
		
		nums[0] = null;
		nums[1] = new int[]{1,2,3};
		nums[2] = new int[]{1};
		
		nums[0] = nums[1];
		
//		System.outsrintln(nums[2].length);
//		String[] strArray = { "The", "quick", null, "brown", "fox" };
//
//		for (int i = 0; i < strArray.length; i++) {
//			if (strArray[i] != null) {
//				System.out.print(strArray[i].length() + " ");
//			}
//		}
//
//		System.out.println("END");
//
//		method1(new int[] { 1, 2, 3 });

//		method1(1, 2, 3, 45, 65, 3, 3);
	}

	public static void primArrays(String[] args) {
		// double[] scores = new double[10];

		int[] myInts = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

		double[] scores = { 99, 0, 78, 87, 56, 76, 56 };

		char[] characters = { 'a', 'r', 'r', 'r', 'a', 'y' };

		boolean[] bools = { true, false, true, false, false };

		Object[] objs = { null, null, new Object() };

		double sum = 0.0;

		for (int i = 0; i < scores.length; i++) {
			// sum += scores[i];
			sum = sum + scores[i];
		}

		double average = sum / scores.length;

		System.out.println(average);
	}
}
